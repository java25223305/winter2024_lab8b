public enum Tile {
    BLANK("-"),
    HIDDEN_WALL("-"),
    WALL("W"),
    CASTLE ("C");

    private final String name;

    Tile(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
