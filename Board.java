import java.util.Random;
public class Board {
    private final int amountOfRowsAndColumns = 5; //Since this is a square, the columns and rows will the the same
    private final Tile[][] grid;
    Random rng = new Random();

    public Board(){
        this.grid = new Tile[amountOfRowsAndColumns][amountOfRowsAndColumns];
        resetBoard();
        for(int i = 0; i < amountOfRowsAndColumns; i++) {
            this.grid[i][rng.nextInt(amountOfRowsAndColumns)] = Tile.HIDDEN_WALL;
        }
    }

    public void resetBoard(){
        for(int i = 0; i < amountOfRowsAndColumns; i++) {
            for(int j = 0; j < grid.length; j++) {
                grid[i][j] = Tile.BLANK;
            }
        }
    }

    public int placeToken(int row, int col){
        if (!(row >= 1 && row <= this.amountOfRowsAndColumns) || !(col >= 1 && col <= this.amountOfRowsAndColumns))
            return -2;
        else 
        if (this.grid[row-1][col-1] == Tile.CASTLE || this.grid[row-1][col-1] == Tile.WALL)
            return -1;
        else
        if (this.grid[row-1][col-1] == Tile.HIDDEN_WALL){
            this.grid[row-1][col-1] = Tile.WALL;
            return 1;
        } 
        else 
            this.grid[row-1][col-1] = Tile.CASTLE;

        return 0;
    }

    public String toString(){
        StringBuilder string = new StringBuilder();
        string.append("  1 2 3 4 5\n");
        for (int i = 0; i < this.amountOfRowsAndColumns; i++){
            string.append((i+1) + " ");
            for(int j = 0; j < this.amountOfRowsAndColumns; j++){
                string.append(grid[i][j].getName() + " ");
            }
            string.append("\n");
        }
        return string.toString();
    }
}
