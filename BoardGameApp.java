import java.util.InputMismatchException;
import java.util.Scanner;

public class BoardGameApp {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Board board = new Board();
        int numCastles = 5, turns = 8;

        clearScreen();
        System.out.println("Welcome to The Board Game!");
        System.out.print("Press enter to start ");
        scanner.nextLine();
        clearScreen();

        while((numCastles > 0 && turns > 0)) {
            if (numCastles > turns)
                break;

            if(turns == 8)
                System.out.println("Current board: \n\n" + board);
            
            int row = getInputNumber("row", 1, 5);
            int column = getInputNumber("column", 1, 5);
            int defineCase = board.placeToken(row, column);

            while (defineCase < 0) {
                System.out.println(row + "," + column + " is an invalid tile");
                row = getInputNumber("row", 1, 5);
                column = getInputNumber("column", 1, 5);
                defineCase = board.placeToken(row, column);
            }

            if (defineCase == 1){
                clearScreen();
                System.out.println("Current board: \n\n" + board);
                System.out.println("There was a hidden wall on tile " + row + "-" + column);
                turns-=1;
            }

            if (defineCase == 0) {
                clearScreen();
                System.out.println("Current board: \n\n" + board);
                System.out.println("A castle was successfully placed!");
                turns -=1;
                numCastles--;
            }

            System.out.println();
            System.out.println("Castles left: " + numCastles);
            System.out.println("Turns left: " + (turns));
            System.out.println();
        }

        clearScreen();
        System.out.println("Final board: \n\n" + board);
        System.out.println((numCastles == 0) ? "You won!" : "You lost with " + numCastles + " castles left to place");
        scanner.close();
    }

     private static int getInputNumber(String inputName, int lowerBound, int upperBound) {
        boolean isInputAnInt = false;
        int number = 0;
        while (!isInputAnInt) {
            try {
                System.out.print("Enter " + inputName + " number (" + lowerBound + "-" + upperBound + "): " );
                number = scanner.nextInt();
                isInputAnInt = true;
            } catch (InputMismatchException ime) {
                System.out.println("Must input a number");
            } finally {
                scanner.nextLine(); //consume int line and prevent infinite loop
            }
        }
        return number;
    }

    private static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
